# Resources

## Guides
- **[markdownguide.org](markdownguide.org)** _A free and open-source reference guide that explains how to use Markdown._
- **[jsonapi.org](jsonapi.org)** _A specification for building APIs in JSON._
* [ ] **[serversforhackers.com](serversforhackers.com)** _Teaching the server tech you need for development and production._

## Tutorials
* [ ] **[howhttps.works](howhttps.works)**
* [ ] **[Time Complexity](adrianmejia.com/blog/2018/04/05/most-popular-algorithms-time-complexity-every-programmer-should-know-free-online-tutorial-course/)**
* [ ] **[freecodecamp.org](freecodecamp.org)** _We have thousands of coding lessons to help you improve your skills._

## Web Resources
- **[webslesson.info](webslesson.info)**
- **[codepad.co](codepad.co)** _Codepad is a place for developers to share code snippets._

## PHP
- [ ] **[PHP Tutorial](https://developer.hyvor.com/tutorials/php/introduction)**
- **[phpschool.io](phpschool.io)** _A dedicated community based learning platform that will teach you the core skills in PHP._
- **[phpdelusions.net/pdo](phpdelusions.net/pdo)** _(The only proper) PDO tutorial_

#### Laravel
- [ ] **[laravelcoreadventures.com](laravelcoreadventures.com)** _Explore the Laravel core from the safety of your home with this free video series. _
- [ ] **[eloquentbyexample.com](eloquentbyexample.com)** _An online course to Learn the Tools and Functions key to a Professional
Workflow and Understanding of the Laravel Eloquent Ecosystem._

#### Master
- **[phpbestpractices.org](phpbestpractices.org)**
- **[designpatternsphp.readthedocs.io/en/latest](designpatternsphp.readthedocs.io/en/latest)**
- **[phptherightway.com](phptherightway.com)**
- **[github.com/jupeter/clean-code-php](github.com/jupeter/clean-code-php)**

## CSS
- **[getbem.com](getbem.com)**
- **[cssanimation.rocks](cssanimation.rocks)** _CSS animation articles, tips and tutorials. _

## Web Development
- [x] **[internetingishard.com](internetingishard.com)** _Friendly web development tutorials for complete beginners_
- [ ] **[practicaltypography.com](practicaltypography.com)** _Practical Typography for Web Development_
- [ ] **[freecodecamp.org](freecodecamp.org)** _Learn Web Development_

## Methodologies
_Agile is an iterative approach to project management and software development._
* [ ] **[atlassian.com/agile](atlassian.com/agile)** 
* [ ] **[mountaingoatsoftware.com/agile](mountaingoatsoftware.com/agile)**

- [x] **[S.O.L.I.D. Principles](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design#toc-interface-segregation-principle)**

## TOOLS
* **[prettyci.com](prettyci.com)** _Continuous integration for PHP coding standards._
* **[ikonate.com](ikonate.com)** _fully customisable & accessible vector icons_
* **[iconsvg.xyz](iconsvg.xyz)** __
* **[fontawesome.com](fontawesome.com)** __
* **[editorjs.io](editorjs.io)** __
* **[slatejs.org](slatejs.org)** __
* **[gallery.manypixels.co](gallery.manypixels.co)** __
* **[simpleanalytics.io](simpleanalytics.io)** _Simple, clean, and friendly analytics._

## Practice
1. **[play.battlesnake.io](play.battlesnake.io)**
1. **[adventofcode.com/2018/day/17](adventofcode.com/2018/day/17)**
1. **[codewars.com](codewars.com)**

## Learn by Building
* [ ] **[emulator101.com](emulator101.com)** _Writing an arcade game emulator._
* [ ] **[cstack.github.io/db_tutorial](cstack.github.io/db_tutorial)** _Build a sqlite Database._

## Blogs
1. **[Robert C. Martin (Uncle Bob)](blog.cleancoder.com)** _The Clean Code Blog_
